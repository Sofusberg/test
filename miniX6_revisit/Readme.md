Link: https://sofusberg.gitlab.io/test/miniX6_revisit/


Which MiniX do you plan to rework?

I went back for last week's miniX5. Minix5 is random melody-maker, and through small changes I want to express how one can get involved in 'critical making' when linking the software to culture. 

What have you changed and why?
From playing random melodies, the sequencer now plays the A minor scale in a chronological order. This is a common scale in European music, and by using the slider to slide towards the right, the scale changes into "harmonic minor". It's a scale that uses an interval of a third from sixth to seventh step of the scale, thus mimicing/leaning towards popular Middle Estearn scales. Thus, the user is entangled in 'cultural appropriation'. 

Cultural appropriation is a term that has been used to critize the use of clothing/appearance from other cultures - but this critice only seems to have been pointed towards clothing/appearance. 

Thus this miniX6 is a piece of satire that deals with the question: Why is some form of cultural apprpriation critized, while others ignored? We listen music that has use influences from other cultures every day, but still it hasn't been a subject of critism - Should it? 

How would you demonstrate aesthetic programming in your work?

Aesthetic programmings take the cultural and political paradigms (p. 14, Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020) into consideration when programming. This miniX is placed within a discussion in cultural context, and thus the cultural focus of aesthetic programming is presented in the work. 

What does it mean by programming as a practice, or even as a method for design?
Programming is a tool for creation on the same level of other tools, having its own advantages and disadvantages. 

What is the relation between programming and digital culture?
By taking different cultural context into consideration when programming, the user engages in 'critical making': A process of investigating social influences in technology by considering your own cultural implementation in software (Nynne, Intructor, 18/3-21)

Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?

In aesthetic programming, aesthetics refer to 'polical aesthetics'. By providing a link to the cultural context of 'cultural appropriation', the miniX demonstrates a critical/reflective perspective regarding politics/debate of social justice.
