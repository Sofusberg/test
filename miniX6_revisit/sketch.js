

let polySynth;
let subdivison = 8;
let i = -1;
let indexG
let sliderChance;
let gType;
let chance;

let slider;

let notes = ['A4','B4','C4','D4','E4','F4','G4','A5'];
let gNotes = ['G4','G#4'];

//sounds
let kick;



// playingArray = [];
// playingArray[1] = [playSynth(chance)]
// playingArray[2] = [playSynth(chance)]
// playingArray[3] = [playSynth(chance)]
// playingArray[4] = [playSynth(chance)]
// playingArray[5] = [playSynth(chance)]
// playingArray[6] = [playSynth(chance)]
// playingArray[7] = [playSynth(chance)]
// playingArray[8] = [playSynth(chance)]

// pause = [];


function preload(){

  soundFormats('mp3');
  kick = loadSound('ba_kick.mp3');

}

function setup(){
createCanvas(600,400);
frameRate(4);
polySynth = new p5.PolySynth();

slider = createSlider(0,100,0);
slider.position(200,200);


}

function draw(){
background(240,220);
textSize(10);
text('Slide right to culturally appropriate Middle Eastern music scales', 180, 250);

console.log(frameRate);

//velocity of note
let vel = random(0.4,1);

//time before note starts
let time = 0;

//duration of note
let dur = 0.1

//index value, notes

i++
console.log(i);

//index selection, returns to i = 0 when exceding lenght of notes
if (i == notes.length) {
  i = 0;
}

//possible G notes, determined by slider
if (i == 6){
  polySynth.play(gNotes[indexG],vel,time,dur)
if (indexG == 1){ //ethnic G note
  push();
    rectMode(CORNER);
    fill(255,0,0);
    rect(160,200,30,30,10);
    pop();
    }
} else{ //the rest of the scale
polySynth.play(notes[i],vel,time,dur)
}


//retrieving slider value
let val = slider.value();

//value over 50 makes scale ethnic instead of "european"
if (val > 50){
indexG = 1; //ethnic scale

kick.setVolume(0.5, 0.2)
if (i == 0 || i == 2 || i == 4 || i == 6){
  kick.play();
  }
} else{indexG = 0}; //european scale


















//mapped slider value determins the chance of hittng note A
// playSynth(chance);

//draw () ending bracket
}




// function playSynth(chanceA){
// console.log(i);
// //velocity of note
// let vel = random(0.4,1);
//
// //time before note starts
// let time = 0;
//
// //duration of note
// let dur = 0.1
//
// if (random (1) < chanceA){
//
//   polySynth.play('A4',vel,time,dur)
//   push();
//   rectMode(CORNER);
//   fill(255,0,0);
//   rect(160,200,30,30,10);
//   pop();
// } else if (random(1) > chanceA){
//   polySynth.play(random(notes), vel, time, dur)
//   }

// }
