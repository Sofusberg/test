**Describe your program and what you have used and learnt.¨**

MiniX2: https://sofusberg.gitlab.io/test/MiniX2_emojis/



By moving the curser along Y-axis, the user will see the face transformate from a white to black male. The software thus contains to faces, emojis.

The transformation is done using conditional statements that seperate the canvas into sections. Each sections has its own arguments in terms of shape and colour, making the skin tone and lips change. 

During the programming, I discovered that I probaliy should have used map() to make a smooth fade between colours/shapes. I learned this through the programming of the eye's movement. 


**– How would you put your emoji into a wider social and cultural context that
concerns a politics of representation, identity, race, colonialism, and so on? (Try
to think through the assigned reading and your coding process, and then expand
that to your own experience and thoughts - this is a difficult task, you may need
to spend some time thinking about it).**

I found myself worrying about the fact that my piece of software visually represents a black person. Due to the simplicity of the geometrical objects in P5.js, I found myself protraying a black person from stereotypes: Big lips, pointy hair. This of course makes me worry about whether my software is discriminating. I had no worry in terms of the white face. 

Thus, the two faces represent discursive views in terms of race: People of black skin (minorities in general) have to be protrayed in a non disctructive way that avoids suporting negative stereoptypes or stories, such as 'black facing'. 
Because he 'white face' has by Deleuze and Guattari been described as an 'imperal machine' that mocks "degrees of deviance to the White man’s face…”, the attempt to capture the very features of the black man's face pose a threat of portraying it with links to racism and slavery. The white face doesn't have a story of being supressed, and thus the face has no features that pose a serious threat of discriminating.  
