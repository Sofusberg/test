
let polySynth;
let subdivison = 8;
let i = 0;
let sliderChance;
let chance;

let slider;

notes = ['B4','C4','D4','E4','F4','G4'];


// playingArray = [];
// playingArray[1] = [playSynth(chance)]
// playingArray[2] = [playSynth(chance)]
// playingArray[3] = [playSynth(chance)]
// playingArray[4] = [playSynth(chance)]
// playingArray[5] = [playSynth(chance)]
// playingArray[6] = [playSynth(chance)]
// playingArray[7] = [playSynth(chance)]
// playingArray[8] = [playSynth(chance)]

// pause = [];


function setup(){
createCanvas(600,400);
frameRate(5);
polySynth = new p5.PolySynth();

slider = createSlider(0,86,0);
slider.position(200,200);


}

function draw(){
background(240,220);
textSize(10);
text('Chance of hitting the basic note "A"', 200, 250);

console.log(frameRate);


// i += 1
//
// if (i == 8){
//   i = 1; //returns to first array
// }


//retrieving slider value
let val = slider.value();
//mapping slider numbers to numbers for chance
sliderChance = map(val,0,86,0,0.86);
let chance = 0.14 + sliderChance; // chance of hitting A note, the basic tone of the scale
console.log(chance);

//mapped slider value determins the chance of hittng note A
playSynth(chance);

//draw () ending bracket
}


function playSynth(chanceA){
console.log(i);
//velocity of note
let vel = random(0.4,1);

//time before note starts
let time = 0;

//duration of note
let dur = 0.1

if (random (1) < chanceA){

  polySynth.play('A4',vel,time,dur)
  push();
  rectMode(CORNER);
  fill(255,0,0);
  rect(160,200,30,30,10);
  pop();
} else if (random(1) > chanceA){
  polySynth.play(random(notes), vel, time, dur)
  }

}
