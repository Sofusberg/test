Link to the software: https://sofusberg.gitlab.io/test/miniX5_generative/

– What are the rules in your generative program? Describe how your program
performs over time? How do the rules produce emergent behavior?

This program is a random melodi generator, in the scale of a minor. 
The two rules are based on random() and distinguishes between the basic note A and rest of the notes in the scale. Thus, the slider increases/decreases the chance of the software playing A - the more A's, the more clear it becomes, that the melodi is played using a minor. 

Emergant behvaior exist within the chance of playing A in comparrence to the rest of the notes: If the chance of playin A increases, the chance of playing one of the remaining notes decreases. 
If the slider is let alone, the chances will not change over time. The noise() function could indeed have been used to automate different chances over time. 


– What role do rules and processes have in your work?

It assigns the basic note A bigger or smaller role, meaning it gets easier or harder to tell which scale the music is performed in. 


– Draw upon the assigned reading, how does this MiniX help you to understand the
idea of “auto-generator” (e.g. levels of control, autonomy, love and care via
rules)? Do you have any further thoughts on the theme of this chapter?

"That said, there is an alternative political potential here in the way an adaptive complex
organism can assemble itself “bottom-up,” without a central “top-down” command and
control mechanism. 34 This demonstrates “revolutionary” potential when it becomes
impossible to predict the direction change will take, and whether it will fall into a higher level
of order or disintegrate into chaos." (p.136, Soon et. al.) 
This quote makes me reflect upon the whole process of producing a generative program. Even though the choice of note can assemble itself "bottom up" when the software is started, this is only true due the fact, that rules have been programmed to determine chance "top down" by me. The fact that a conditional statement is required in this weekly miniX proves the fact, that the software has no complete freedom: Its output is determined by conditions set by me. 
