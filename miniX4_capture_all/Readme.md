Link: 
https://sofusberg.gitlab.io/test/miniX4_capture_all/

![screendump](/uploads/f0eaff5561c3a3c794bb6db3e725e804/screendump.png)

– Provide a title for and a short description of your work (1000 characters or less)
as if you were going to submit it to the festival.

This piece of work is called: 'Associations for sale'. 
It's a piece of software that mimics the algrorithmics behind the tracking of user data for advertisements. By hovering the mouse over the different products, the computer generates recommendations for related products to buy. When both products have been hovered, the computer use associations from both products to recommend a new product. Thus, the computer learns about the user's interest through mouse tracking. 



– Describe your program and what you have used and learnt.
For this miniX, I have played around with conditional statements to create a 'dramaturical structure' to the software. Through interaction with the software, the user's behavior is transformed into recomended items for purchase. 

I used arrays to create advertisements with changing images, which gives the site some dynamic. 

– Articulate how your program and thinking address the theme of “capture all.”
This piece of software mimics the computer targeting advertisements for the user, based on their behavior on the web. 
– What are the cultural implications of data capture?
That every human behavior can be associated into products for sale. The computer tries to guess the intentions behind different online behavior. 
