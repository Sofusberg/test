//arrays
let plants = [];
let datingSites =[];


//Syncs the tempo of advertisement images
let pauseCount = 0;

//watering can + plants
let canCount = 0;
let mouseAlpha = 0;
let canIteration = 0;


//chewing gum + dating sites
let gumCount = 0;
let pauseCountGum = 0;
let mouseAlpha1 = 0;
let gumIteration = 0;


function preload(){
soundFormats('mp3');
elevatorMusic = loadSound('elevator_music.mp3')


wateringCan = loadImage('watering_can.jpg');
//plants array
plants[0] = loadImage('plant1.jpg');
plants[1] = loadImage('plant2.jpg');
plants[2] = loadImage('plant3.jpg');


chewingGum = loadImage('chewinggum.jpg')
//dating sites array
datingSites[0] = loadImage('datino.png');
datingSites[1] = loadImage('bridesandlovers_logo.png');
datingSites[2] = loadImage('freedating.png');


//bouquet
bouquet = loadImage('bouquet.png');


//preload() ending bracket
}

function setup(){
frameRate(10);
createCanvas(600,400);
elevatorMusic.play();
//setup() ending bracket
}

function draw(){
background(255);



//wateringCan mouse interaction//
if (mouseX >= 190 && mouseX <=290 && mouseY >= 30 && mouseY <=130){
canCount += 1;
console.log(canCount);
mouseAlpha += 8,5;

}

/*30 canCount iterations over waterCan starts advertisements of plants
and the time (pauseCount) between random selection of picture*/
if (canCount > 30){
pauseCount += 1;
if (pauseCount <= 10)
{
let plant = random(plants);
image(plant,100,300,100,100);
}
}

//pauseCount resets every 10 frames
if (pauseCount == 10){
pauseCount = 0;
}
console.log(pauseCount);

//canCount increases alpha value for mouse ellipse
push();
fill(0,0);
stroke(random(255),255,0,mouseAlpha)
ellipseMode(CENTER);
strokeWeight(10);
ellipse(240,80,200);
pop();

//displaying text, dertimed by pauseCount
if (pauseCount == 2 || pauseCount == 4 || pauseCount == 6 || pauseCount ==8){

fill(0,255);
textSize(32);
text('This person loves plants, right??',20,25);
} else{ fill(0,0);
textSize(32);
text('This person loves plants, right??',20,30);
}



/*chewing gum mouse interaction */

//chewing gum mouse interaction//
if (mouseX >= 300 && mouseX <=400 && mouseY >= 140 && mouseY <=240){
gumCount += 1;
console.log(gumCount);
mouseAlpha1 += 8,5;

}

/*30 gumCount iterations over image of chewing gum gym starts advertisements of dating sites
and the time (pauseCount) between random selection of picture*/
if (gumCount > 30){
pauseCountGum += 1;
if (pauseCountGum <= 10)
{
let datingSite = random(datingSites);
imageMode(CORNER);
image(datingSite,300,300,230,100);
}
}
//pauseCount resets every 10 frames
if (pauseCountGum == 10){
pauseCountGum = 0;
}


//circle around product, mouseAlpha increased by gumCount
push();
fill(0,0);
stroke(random(170,255),0,200,mouseAlpha1)
ellipseMode(CENTER);
strokeWeight(10);
ellipse(350,190,200);
pop();



//displaying text

if (pauseCountGum == 2 || pauseCountGum == 4 || pauseCountGum == 6 || pauseCountGum ==8){
fill(0,255);
textSize(32);
text('Person who looks for date??',180,275);
} else{ fill(0,0);
textSize(32);
text('Looking for date??',305,250);
}




/*product pictures*/

//watering can
push();
imageMode(CORNER);
image(wateringCan,190,30,100,100);
pop();



//chewing gum
push();
imageMode(CORNER);
image(chewingGum,300,140,95,95);
pop();



/* static layout */

//advertisement boxes
fill(255,0);
rect(20,300,560,200,2);



//product areas
fill(0,0);
strokeWeight(3);
rect(190,30,100,100,2);
// rect(300,30,100,100,2);
// rect(190,140,100,100,2);
rect(300,140,100,100,2);

console.log(mouseX,mouseY);



/*when gumCount and canCount are both > 30 AND a sequence of 10 iterations
for pauseCount have been meet 3 times, a bouquet of flowers will be shown */

if ((canCount > 30 && gumCount > 40)||(canCount > 40 && gumCount > 30)){
push();
filter(BLUR,3);
fill(120);
rect(600,400)
pop();
imageMode(CORNER);
image(bouquet,50,50,200,200)
textSize(32);
fill(0);
text('BOUQUET FOR DATE!??!?',random(90,100),random(290,300));
elevatorMusic.stop();

}


push();
strokeWeight(3);
fill(0,0),
rect(0,0,600,400);
pop();

//draw ending bracket
}
