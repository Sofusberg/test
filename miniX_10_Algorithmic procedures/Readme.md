Individual flowchart: 
Using a flowchart, I have represented the conditional logic of my miniX6. The miniX conistst of a loop playing the natural A minor scale over and over. When the slider is dragged above 50 percent, the scale changes from A minor natural to A minor harmonic. A minor harmonic is used to mimic middle eastern musical scales. 


![own_flowchart](own_flowchart.png)


**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

When flowcharting, it can be easy to get caught up in the little details and start pondering how exactly things should be executed on the code level in our program. For the sake of creating a more fruitful idea-generating environment, we therefore decided to create more overall flowcharts, showcasing the general ideas of our concepts, instead of lingering too much on syntactical detail.

**What are the technical challenges facing the two ideas and how are you going to address these?**


When brainstorming and creating the flowchartes, we haven’t focused on the technical aspects of the program, but rather the conceptual thinking. We wanted to create two superficial ideas and let the design become more specific, later in the design process. 

**In which ways are the individual and the group flowcharts you produced useful?**
Whilst brainstorming, a lot of abstract ideas and concepts are thrown around in the group. Creating a flowchart is a nice way to visualise and concretise the ideas and get a sense that all group members are on the same page in terms of the more concrete execution of the idea.

In terms of my own experience, flowcharts really help create an overview of the code: Often, the code is written in a manor forcing the reader to scroll up and down. A flowchart helps you know where to look for the next logical step in the code and how the logic itself works. 


**General description of our ideas: **
Link to flowcharts: https://miro.com/app/board/o9J_lJQwdWY=/

**flowchart 1: Artwork generator**

![Flowchart1](Flowchart1.png)

The artwork generator is a program in which the computer is given an api containing photos organized into categories. The computer has also been given templates of larger photos/symbols. The computer will then create a photo mosaic of the larger photos/symbols made of many of the smaller photos from the api. In this way we are able to create a mosaic containing two opposite ideas/political statements/etc. For example, one artwork could be a photo of the pride flag, made by many small photos of Jehovah's Witnesses. 

**Flowchart 2: Data query - The cultural depth of words and their synonyms. **

![Flowchart2](Flowchart2.png)

A user sends a request for a word to check its connected synonyms. The sotware clicks on the first available synonym and proceeds to the page the chosen synonym. The first synonym of the newly opened word description is then clicked, and the mentioned process iteration a prefixed number of times. 

The software deals with associations, and how different words can essentially frame the same object or phenomenon by carrying different cultural charges.  
The used words are retrieved from API’s containing words and the associations. 
For instance: 
https://wordassociations.net/en/api	
https://www.wordsapi.com/



	
