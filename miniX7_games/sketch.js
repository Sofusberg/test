
//snake variables
let head = [];
let instances = 3; //number heads added created in setup();
let limit = 5;
let x;
let y;
let radius;
let lastIndex;

//edible variables
let edible = [];
let ex;
let ey;
let eRadius;




function setup(){
createCanvas(600,400);

//setup heads
for (i = 0; i < instances; i++){
x = 100 + 20 * i;
y = 100;
radius = 10;
b = new Head(x,y,radius);
head.push(b);
}


//setup() ending bracket
}




//keyCode, head movement, p5-library

function keyPressed(){
  //left arrow
  if (keyCode === 37){
    x = x - 20
newHead();

  } else if (keyCode === 38){
    y = y - 20
 newHead();
 //right arrow
} else if (keyCode === 39){
  newHead();
  x = x + 20;
  //down arrow
} else if (keyCode === 40){
  newHead()
  y = y + 20;
}

}

//function to add new head to array 'head'
function newHead(){
  b = new Head(x,y,radius);
    console.log(head);
    head.push(b);
}




function draw(){
  background(120);

//show heads
for (i = 0; i < head.length; i++){
  head[i].show();
  }

//variable returning the last index value in array 'head'
  lastIndex = head.length-1;


  //head, index limit
  if (head.length > limit){
    head.splice(0,1);
  }





//edibles
for (let i = 0; i < 5; i++){
ex = random(width);
ey = random(height);
eRadius = 12;
e = new Edible(ex,ey,eRadius);
edible.push(e);
edible[i].eShow();

}

//distancecheck between edible and snake
for (let i = edible.length-1; i >= 0; i--){
  if (edible[i].eDistanceCheck()){
edible[i].splice(i,1);
head.splice(0,1);
    }
  }




//draw() ending bracket
}




class Head{
  constructor(x,y,radius){
  this.x = x;
  this.y = y;
  this.radius = radius;

  //variables to check encounter with Edible class
  this.ex = ex;
  this.ey = ey;
  }

show(){
  ellipse(this.x,this.y,this.radius*2);
}

// distanceCheck(){
//   //distance between snake and edible
// let d = dist(this.x,this.y,this.ex,this.ey);
// if (d < this.radius){
//   limit++; // encreases length of head array => length of snake
//   console.log("Gotcha!")
//     }
//   }

}





class Edible{
  constructor(ex,ey,eRadius){

    //edible variables
    this.ex = ex;
    this.ey = ey;
    this.eradius = eRadius;

    //snake variables
this.x = x;
this.y = y;
  }
  eShow(){
    ellipse(this.ex,this.ey,this.eradius*2);
  }

  eDistanceCheck(){
    //distance between snake and edible
  let d = dist(this.ex,this.ey,this.x,this.y);
  if (d < this.eradius){
    limit++; // encreases length of head array => length of snake
    console.log("Gotcha!")
    console.log(limit);
  } 

  }
}
