let opacity = 255;
let opacityNail = 255;
let nailX = 45;
let opacityText = 255;

let num = 9;
let threehundredsixtydegrees= 360;
let ellipseX = 0;
let variabelX = 10

function setup(){
createCanvas(windowWidth,windowHeight);
frameRate(9);


}

function draw(){
background(120,240);
drawElements();
board();

textSize(32);
fill(0,opacityText)
text('Setting', 10, 30);
fill(0,opacityText);
text('Up', 10, 60);
fill(0, opacityText);
text('Software', 10, 90);


//opacityText fading visible or nonvisible//
if (frameCount == 1 || frameCount == 3 || frameCount == 5){
  opacityText = 255;
} else {
  opacityText = 0;
}



//opacityText fading in and out//
// if (frameCount < 5 || frameCount > 10){
//   opacityText += 36
// } else if (frameCount > 6){
//   opacityText = 0;
// }


/*iterative loop,every 8 and 16 frames resets framecount and changes direction of
rotation of hammer*/
if (frameCount == 8 || frameCount == 16){
//Switching between + and - for rotation//
threehundredsixtydegrees = threehundredsixtydegrees *-1;
}

if (frameCount == 16){
frameCount = 0;
}


//X value grows and decreases upon every four frames//
if(frameCount == 4){
variabelX = variabelX*-1
}

//x value//
ellipseX = ellipseX + variabelX


print(frameCount);

//draw ending bracket//
}

//defining functions//

//hammer//
function drawElements() {
push();
//moves [0,0] to center
translate(width/2,height/2);
let cir = threehundredsixtydegrees/num*(frameCount%num);
rotate(radians(cir));
noStroke();
fill(255,opacity);
rect(0-20,50,60,20);
rect(0,0,20,80,20);
pop();
stroke(255,255,0);

}

//text//





//board//
function board(){
translate(width/2,height/2);
noStroke(0);
fill(255);
rect(-60,65,44,20,2)

//nails//
noStroke();
fill(255,opacityNail)
rect(-40,nailX,2,20)
rect(-44,nailX,10,2,10)


// //nail pushed down//
if (frameCount == 1){
nailX = 60;
}


//nail pushing up//

if (frameCount > 3 && frameCount<8){
nailX -= 3.3;
}

//nail pushed back up//
if (frameCount == 8){
nailX = 45;
}



}



function windowResized(){
resizeCanvas(windowWidth,windowHeight);
}
