**Questions to think about (ReadMe):**
Describe your throbber design, both conceptually and technically.

https://sofusberg.gitlab.io/test/miniX3_Throbber/


Conceptually, my throbber reflects a hammer and a nail: Metaphorically, It uses an activity from construction fields to visualize the concept of "building" the program for users to enter. The metaphor makes the wait tangible, since everyone understands that building and construction a space to enter takes time. 

Technically, the function frameCount() makes the animation possible. The frameCount resets upon every 16 frames, and within that "timespan", conditional statements move the objects. 
For instance, the direction of the rotatation of the hammer changes every 8 frame:

/*iterative loop,every 8 and 16 frames resets framecount and changes direction of
rotation of hammer*/
if (frameCount == 8 || frameCount == 16){
//Switching between + and - for rotation//
threehundredsixtydegrees = threehundredsixtydegrees *-1;
}



–** What do you want to explore and/or express?**

I wanted to explore how one can programe loops within an overall "timespan" of 16 frames. 

**– What are the time-related syntaxes/functions that you have used in your
program, and why have you used them in this way? How is time being
constructed in computation (refer to both the reading materials and
your coding)?**


In terms of time in my code, I used frameCount() for setting up the overall loop and loops within. 
FrameCount () is not only used to make things move, but also to wait moving: 

Here, the X value of the nail decreases after a wait of 3 frames, making the nail stand still during the three frames. 

//nail pushing up//

if (frameCount > 3 && frameCount<8){
nailX -= 3.3;
}

Time in computation: "Mechanical clocks also allowed for the unification of different
	time cycles and scales", Lammerant (p. 91): This is expressed in my code, where the frameCount makes the time repeat itself in the exact same way again and again: Even allowing loops within itself. 

At the same time, "the internal crystal clock drives the cycles according to which the
	processor works."(p.91)  This means, that no matter how the loops seem to be executed, the code still stays true to the internal clock. 

" This produces a single cyclical, and completely linear, time rhythm in the hardware. In contrast to the earthly or natural cycles it is without any seasonal difference or interference between several cycles." (Lammerant s.91-92). This potentially means that the created loop is its own beginning and end, and what you see it what you get - forever, until stopped. 


**– Think about a throbber that you have encounted in digital culture, e.g. for
streaming video on YouTube or loading the latest feeds on Facebook, or waiting
for a payment transaction, and consider what a throbber communicates, and/or
hides? How might we characterize this icon differently?**



The throbber that first comes to mind is the spinning Master Roshi from the video game Dragon Ball Z: Budokai 2 to Playstation 2. With the analoge stick, you could spin him around and distract yourself from the fact you are waiting for a loading screen to finish. 
The distractions is the main point here: A loading screen is essentially a part of the game that forces the player to wait, and by making the throbber interactive, you spare the player from boredom. Reminding us of the fact that many games are made to entertain us at ALL times. 
