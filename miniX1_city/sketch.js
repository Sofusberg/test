
function setup(){
createCanvas(600,400);
colorMode(RGB,100);
background(0,100,100);
frameRate(10);
}

function draw(){


  //sol//
  fill(255,100,0,4);
  noStroke();
  ellipse(520,60,50,50);

//mørk rektangel = nat//
fill(0,0,255,0.2);
noStroke();
rect(0,0,600,400);


//natteliv ellipse //
fill(random(100),0,100);
ellipse(360,400,300,100);



//skyer, tegnes med musens bevægelse//
fill(255,10);
noStroke();
ellipse(mouseX,mouseY,50,50);

//bygninger og vinduer//



//random gråtone på bygninger//
fill(random(255));
stroke(0,0,0);
//bagerst bygning//
rect(420,200,100,500);

//vindur bagerste bygning//
rect(430,240,30,30);
rect(480,240,30,30);

rect(430,290,30,30);
rect(480,290,30,30);

rect(430,340,30,30);
rect(480,340,30,30);

rect(430,390,30,30);
rect(480,390,30,30);


//kabel bagester bygning til andenforreste bygning//
line(480,180,550,280);

//de to pæle//
line(480,165,480,200);

line(550,220,550,320);



//andenforreste bygning//
rect(500,300,200,500);


//vinder forreste bygning//
rect(520,320,35,35);
rect(580,320,35,35);

rect(520,390,35,35);
rect(580,390,35,35);



//forreste bygning//
rect(0,0,320,600);

//vinduer foreste bygning//

rect(50,50,50,50);

rect(200,50,50,50);

rect(50,200,50,50);

rect(200,200,50,50);


rect(50,350,50,50);

rect(200,350,50,50);





}
