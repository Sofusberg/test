MiniX1
https://sofusberg.gitlab.io/test/miniX1_city/

**– What have you produced?**
A city:
The user is able to draw clouds by moving the curser over the background. This is made using transparancy to make the user able to draw transparent to solid clouds. 
The backgground will slowly shift from day to night. This is also made using transparancy to give the effect of a slow shift. 
The buildings randomly change color within the grayscale, and the "lights" from the city randomly change colours within the RGB-scale. 


**– How would you describe your first independent coding experience (in relation to
thinking, reading, copying, modifying, writing code, and so on)?**

My first coding experience hugely relies on following the first couple of tutorials from Daniel Schiffman's Youtube channel. I found that taking the time to understand the code presented makes me able to modify it on my own and to be creative. 

**– How is the coding process different from, or similar to, reading and writing text?**
In both coding and writing, it's required to understand the basic language you are trying to use for producing/reading. 
A difference is, that coding is a tool for creating a representation. Writing text can on its own be a final product. 
In coding, there's no room for mistakes: The presentation can't run with even minor issues with the syntax. This is different when writing text, since you can still read the and understand it despite minor mistakes in the language. 

**– What does code and programming mean to you, and how does the assigned
reading help you to further reflect on these terms?**

It gives an understanding of both the syntax of p5.js itself, but also the context around it: That its open source, and that coding can be seen becoming a common skill or literacy, 'expanded literacy'. 
